package eu.saramak.example.volley.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.saramak.example.volley.DefaultHttpClientFragment;
import eu.saramak.example.volley.DefaultHttpClientFragmentOk;
import eu.saramak.example.volley.PerformanceBaseFragment;
import eu.saramak.example.volley.VolleyImageLoaderFragment;
import eu.saramak.example.volley.VolleyTestDetailFragment;
import eu.saramak.example.volley.VolleyTestGsonFragment;
import eu.saramak.example.volley.VolleyTestPolicyFragment;
import eu.saramak.example.volley.VolleyTestPriorityFragment;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class URLContent {

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

	static {
		// Add 3 sample items.
		addItem(new DummyItem(DummyItem.VOLLEY_NO_CACHE,  "http://damp-chamber-4526.herokuapp.com/pets/","Volley no cache", VolleyTestDetailFragment.class));
		addItem(new DummyItem(DummyItem.VOLLEY_CACHE,  "http://damp-chamber-4526.herokuapp.com/pets/clist","Volley with cache", VolleyTestDetailFragment.class));
		addItem(new DummyItem(DummyItem.DEFAULT_HTTP,  "http://damp-chamber-4526.herokuapp.com/pets/clist","Default http", DefaultHttpClientFragment.class));
		addItem(new DummyItem(DummyItem.DEFAULT_HTTP_OK,  "http://damp-chamber-4526.herokuapp.com/pets/clist", "Default http 2",DefaultHttpClientFragmentOk.class));
		addItem(new DummyItem(DummyItem.VOLLEY_IMEGE_LOAD,  "https://lh5.googleusercontent.com/-Pcm6ZVPmIjc/Um8CzusvBMI/AAAAAAAACeg/hGrRdGZDzt8/w2048-h1365/DSC02733.JPG", "Image Load",VolleyImageLoaderFragment.class));
		addItem(new DummyItem(DummyItem.VOLLEY_PRIORITY,  "http://saramak.pl/volleyt/","Volley priority", VolleyTestPriorityFragment.class));
		addItem(new DummyItem(DummyItem.VOLLEY_RECONNECT_POLICY,  "http://damp-chamber-4526.herokuapp.com/pets/somefail/5","Volley reconnect policy", VolleyTestPolicyFragment.class));
		addItem(new DummyItem(DummyItem.VOLLEY_GSON_TEST, "http://damp-chamber-4526.herokuapp.com/pets/clist","Volley gson", VolleyTestGsonFragment.class));
	}

	private static void addItem(DummyItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	/**
	 * A dummy item representing a piece of content.
	 */
	public static class DummyItem {
		public static final String VOLLEY_NO_CACHE = "1";
		public static final String VOLLEY_CACHE = "2";
		public static final String DEFAULT_HTTP = "3";
		public static final String DEFAULT_HTTP_OK = "4";
		public static final String VOLLEY_IMEGE_LOAD = "5";
		public static final String VOLLEY_PRIORITY = "6";
		public static final String VOLLEY_RECONNECT_POLICY = "7";
		public static final String VOLLEY_CANCELING = "8";
		public static final String VOLLEY_GSON_TEST = "9";
		public String id;
		public String content;
		public Class<? extends PerformanceBaseFragment> class1;
		public String url;

		public DummyItem(String id, String url, String content, Class<? extends PerformanceBaseFragment> class1) {
			this.id = id;
			this.content = content;
			this.url = url;
			this.class1 = class1;
		}

		@Override
		public String toString() {
			return content;
		}
	}
}
