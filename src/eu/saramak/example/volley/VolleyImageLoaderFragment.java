package eu.saramak.example.volley;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.ViewSwitcher;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import eu.saramak.example.volley.cache.BitmapLruImageCache;
import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.PerformanceDisplayer;
import eu.saramak.example.volley.performance.RequestChecker;


public class VolleyImageLoaderFragment extends PerformanceBaseFragment
		implements OnClickListener, OnCheckedChangeListener {

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestDetailFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;


	private Switch mSwitchButton;

	private ProgressBar mImageLoadingProgressBar;

	private NetworkImageView mVolleyImageView;

	private RequestQueue mQueue;

	private ViewSwitcher mResultSwitcher;

	private ImageLoader mImageLoader;

	public VolleyImageLoaderFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_image_test1,
				container, false);
		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);
		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mImageLoadingProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_image_test);		
		mResultSwitcher = (ViewSwitcher)rootView.findViewById(R.id.result);
		mStartButton.setOnClickListener(this);
		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);
		
		//mięsko 
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		mVolleyImageView = (NetworkImageView)rootView.findViewById(R.id.imgtest);
		
		
		return rootView;
	}
	
	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		final RequestChecker r = mPerformanceDisplayer.start();
		mImageLoader = new ImageLoader(mQueue,new BitmapLruImageCache(10*1024*1024)){
			@Override
			public ImageContainer get(String requestUrl, ImageListener listener) {
				
				return super.get(requestUrl, new PerformanceImageListener(listener, mPerformanceDisplayer, r));
			}
			
		};
		String url = mItem.url;
		mVolleyImageView.setImageUrl(url, mImageLoader);
		mVolleyImageView.setVisibility(View.VISIBLE);
		mImageLoadingProgressBar.setVisibility(View.GONE);
	}
	private static class PerformanceImageListener implements ImageListener {

		private ImageListener mListener;
		private PerformanceDisplayer mPerformanceDisplayer;
		private RequestChecker r;

		public PerformanceImageListener(ImageListener listener, PerformanceDisplayer performanceDisplayer, RequestChecker r) {
			this.mListener = listener;
			this.mPerformanceDisplayer = performanceDisplayer;
			this.r = r;
			
		}

		@Override
		public void onErrorResponse(VolleyError arg0) {
			mListener.onErrorResponse(arg0);
		}

		@Override
		public void onResponse(ImageContainer arg0, boolean arg1) {
			mListener.onResponse(arg0, arg1);
			mPerformanceDisplayer.end(r);
		}
		
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mResultSwitcher.showNext();
		} else {
			mPerformanceDisplayer.showResults(mListView);
			mResultSwitcher.showPrevious();
		}
	}
}
