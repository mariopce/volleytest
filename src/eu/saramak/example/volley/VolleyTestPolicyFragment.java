package eu.saramak.example.volley;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;

import com.android.volley.Cache.Entry;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Priority;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.PerformanceDisplayer;
import eu.saramak.example.volley.performance.RequestChecker;


public class VolleyTestPolicyFragment extends PerformanceBaseFragment implements
		OnClickListener, OnCheckedChangeListener {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestPriorityFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;


	private RequestQueue mQueue;
	private ArrayAdapter<String> mResultAdapter;

	
	private Switch mSwitchButton;

	private Switch mRetryPolicy;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public VolleyTestPolicyFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
		VolleyLog.DEBUG= true;
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_detail_retry,
				container, false);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		
		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);
		
		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mRetryPolicy = (Switch) rootView.findViewById(R.id.secound_switch);
		mRetryPolicy.setText("retry");
		mResultAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
		
		mListView.setAdapter(mResultAdapter);
		mStartButton.setOnClickListener(this);
		
		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);
	
		return rootView;
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		StringRequest request1 = getRequest(1);
		request1.setShouldCache(false);
		if (mRetryPolicy.isChecked()){
			request1.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.1f));
		}else{
			mQueue.getCache().clear();
			request1.setRetryPolicy(new DefaultRetryPolicy(2500, 0, 1f));
			
		}
		mQueue.add(request1);
	}

	private StringRequest getRequest(int i) {
		final RequestChecker r = mPerformanceDisplayer.start();
		return new StringRequest(mItem.url, new SuccessListener(mResultAdapter, mPerformanceDisplayer, r), new ErrorDisplayListener(mResultAdapter, mPerformanceDisplayer, r)){
			
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
		           HashMap<String, String> headers = new HashMap<String, String>();
		           headers.put("CUSTOM_HEADER", "Yahoo");
		           headers.put("CUSTOM_HEADER2", "Google");
		           return headers;

			}
			
		};
	}
	private  static class SuccessListener implements Listener<String> {

		private PerformanceDisplayer pd;
		private RequestChecker r;
		private ArrayAdapter<String> mResultAdapter;

		public SuccessListener(ArrayAdapter<String> resultAdapter, PerformanceDisplayer pd, RequestChecker r) {
			this.mResultAdapter = resultAdapter;
			this.pd = pd;
			this.r = r;
			
		}

		@Override
		public void onResponse(String res) {
			mResultAdapter.add("success");
			mResultAdapter.notifyDataSetChanged();
			pd.end(r);
		}
		
	}

	private static class ErrorDisplayListener implements ErrorListener {
		
		private PerformanceDisplayer pd;
		private RequestChecker r;
		private ArrayAdapter<String> mResultAdapter;
		
		public ErrorDisplayListener(ArrayAdapter<String> resultAdapter, PerformanceDisplayer pd, RequestChecker r) {
			this.mResultAdapter = resultAdapter;
			this.pd = pd;
			this.r = r;
			
		}
		@Override
		public void onErrorResponse(VolleyError error) {
			mResultAdapter.add("error status:" + error.getCause());
			mResultAdapter.notifyDataSetChanged();
			pd.end(r);
		}
		
	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mListView.setAdapter(mResultAdapter);
			mResultAdapter.notifyDataSetChanged();
		}else {
			
			mPerformanceDisplayer.showResults(mListView);
			mResultAdapter.notifyDataSetChanged();
		}
	}
}
