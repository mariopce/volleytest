package eu.saramak.example.volley;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;
import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.PerformanceDisplayer;
import eu.saramak.example.volley.performance.RequestChecker;
import eu.saramak.example.volley.pet.JsonPetParser;


public class DefaultHttpClientFragmentOk extends PerformanceBaseFragment implements
		OnClickListener, OnCheckedChangeListener {
	private static final String HTTPKEY_USER_AGENT = "User-Agent";

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestDetailFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;

	private ArrayAdapter<String> mPetsAdapter;

	private Switch mSwitchButton;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DefaultHttpClientFragmentOk() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_detail_test1,
				container, false);

		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);
		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mPetsAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);
		mListView.setAdapter(mPetsAdapter);
		mStartButton.setOnClickListener(this);
		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);
		return rootView;
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		final RequestChecker r = mPerformanceDisplayer.start();
		String url = mItem.url;
		new DownloadJsonAsyncTask(mPerformanceDisplayer, r, mPetsAdapter).execute(url);
	}

	
	private static class DownloadJsonAsyncTask extends AsyncTask<String, Void, Result<List<String>>>{
		private PerformanceDisplayer mPerformanceDisplayer;
		private RequestChecker mRequestSpeed;
		private ArrayAdapter<String> mPetsAdapter;

		public DownloadJsonAsyncTask(PerformanceDisplayer performanceDisplayer, RequestChecker r, ArrayAdapter<String> petsAdapter) {
			this.mPerformanceDisplayer = performanceDisplayer;
			this.mRequestSpeed = r;
			this.mPetsAdapter = petsAdapter;
		}
		
		@Override
		protected Result<List<String>> doInBackground(String... params) {
			Result<List<String>> result = new Result<List<String>>();
			try {
				String url = params[0];
				List<String> list = connect(url);
				result.setResult(list);
			} catch (IOException e) {
				Log.e(TAG, "Exception when read url " + params[0]);
				result.setError(e);
			} catch (JSONException e) {
				Log.e(TAG, "Exception when parsing " + params[0]);
				result.setError(e);
			}
			return result;
		}
		
		
		
		private List<String> connect(String url) throws IOException, JSONException {
			
			HttpClient client = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			request.setHeader(HTTPKEY_USER_AGENT, "set your desired User-Agent");
			HttpResponse response = client.execute(request);

			// Check if server response is valid
			StatusLine status = response.getStatusLine();
			if (status.getStatusCode() != 200) {
				throw new IOException("Invalid response from server: "
						+ status.toString());
			}
			// Pull content stream from response
			HttpEntity entity = response.getEntity();
			InputStream inputStream = entity.getContent();

			ByteArrayOutputStream content = new ByteArrayOutputStream();
			// Read response into a buffered stream
			int readBytes = 0;
			byte[] sBuffer = new byte[512];
			while ((readBytes = inputStream.read(sBuffer)) != -1) {
				content.write(sBuffer, 0, readBytes);
			}
		    // Return result from buffered stream
	        String dataAsString = new String(content.toByteArray());
	        return new JsonPetParser().parse(new JSONArray(dataAsString));
		}
		
		@Override
		protected void onPostExecute(Result<List<String>> result) {
			mPerformanceDisplayer.end(mRequestSpeed);
			mPetsAdapter.clear();
			mPetsAdapter.addAll(result.getResult());
			super.onPostExecute(result);
		}
	}
	public static class Result<T> {
		private Exception e;
		private T result;
		public boolean isError(){
			return e!=null;
		}
		public void setResult(T res) {
			result = res;
		}
		public void setError(Exception e){
			this.e = e;
		}
		public Exception getError(){
			return e;
		}
		public T getResult(){
			return result;
		}
	}
	

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mListView.setAdapter(mPetsAdapter);
			mPetsAdapter.notifyDataSetChanged();
		} else {
			mPerformanceDisplayer.showResults(mListView);
			mPetsAdapter.notifyDataSetChanged();
		}
	}
}
