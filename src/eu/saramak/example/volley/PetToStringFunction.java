package eu.saramak.example.volley;

import com.google.common.base.Function;

import eu.saramak.example.volleyrequest.Pet;

public class PetToStringFunction implements Function<Pet, String> {

	@Override
	public String apply(Pet pet) {
		return pet.name + " " + pet.weight;
	}

}
