package eu.saramak.example.volley.pet;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class JsonPetParser implements PetParser {

	private static final String TAG = null;

	@Override
	public List<String> parse(JSONArray response) throws JSONException {
		List<String> petNames = new ArrayList<String>();
		for (int i = 0; i < response.length(); i++) {
				JSONObject speakerJson = (JSONObject) response.get(i);
				String petName = (String) speakerJson.getString("name");
				petNames.add(petName);
				
		}
		return petNames;
	}
	
}
