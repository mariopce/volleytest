package eu.saramak.example.volley.pet;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

public interface PetParser {

	List<String> parse(JSONArray response) throws JSONException;
	
}
