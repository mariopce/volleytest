package eu.saramak.example.volley;

import android.support.v4.app.Fragment;
import android.widget.ListView;
import eu.saramak.example.volley.dummy.URLContent.DummyItem;
import eu.saramak.example.volley.performance.FragmentPerformanceDisplayer;
import eu.saramak.example.volley.performance.PerformanceDisplayer;
import eu.saramak.example.volley.pet.JsonPetParser;
import eu.saramak.example.volley.pet.PetParser;

public class PerformanceBaseFragment extends Fragment {

	protected PetParser mPetParser;
	protected PerformanceDisplayer mPerformanceDisplayer;
	protected ListView mListView;

	public PerformanceBaseFragment() {
		mPetParser = new JsonPetParser();
		mPerformanceDisplayer = new FragmentPerformanceDisplayer();
	}

	public static PerformanceBaseFragment create(String itemID) {
		if (DummyItem.VOLLEY_CACHE.equals(itemID)
				|| DummyItem.VOLLEY_NO_CACHE.equals(itemID)) {
			return new VolleyTestDetailFragment();
		} else if (DummyItem.DEFAULT_HTTP.equals(itemID)) {
			return new DefaultHttpClientFragment();
		} else if (DummyItem.DEFAULT_HTTP_OK.equals(itemID)) {
			return new DefaultHttpClientFragmentOk();
		} else if (DummyItem.VOLLEY_IMEGE_LOAD.equals(itemID)) {
			return new VolleyImageLoaderFragment();
		} else if (DummyItem.VOLLEY_PRIORITY.equals(itemID)) {
			return new VolleyTestPriorityFragment();
		} else if (DummyItem.VOLLEY_RECONNECT_POLICY.equals(itemID)) {
			return new VolleyTestPolicyFragment();
		} else if (DummyItem.VOLLEY_GSON_TEST.equals(itemID)) {
			return new VolleyTestGsonFragment();
		}
		return null;
	}

}
