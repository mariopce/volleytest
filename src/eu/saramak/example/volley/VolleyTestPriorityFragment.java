package eu.saramak.example.volley;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;

import com.android.volley.Request.Priority;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.PerformanceDisplayer;
import eu.saramak.example.volley.performance.RequestChecker;

public class VolleyTestPriorityFragment extends PerformanceBaseFragment
		implements OnClickListener, OnCheckedChangeListener {
	private static final String PRIO_CANCEL_TAG = "prio";

	private static final String CANCEL_KEY = "Cancel";

	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestPriorityFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;

	private RequestQueue mQueue;
	private ArrayAdapter<String> mResultAdapter;

	private Switch mSwitchButton;

	private Switch mCancelAfter3Button;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public VolleyTestPriorityFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
		VolleyLog.DEBUG = true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_detail_retry,
				container, false);

		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());

		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);

		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mCancelAfter3Button = (Switch) rootView.findViewById(R.id.secound_switch);
		mCancelAfter3Button.setText("cancel after 3 request");
		mResultAdapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1);

		mListView.setAdapter(mResultAdapter);
		mStartButton.setOnClickListener(this);

		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);

		return rootView;
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		

		StringRequest request1 = getRequest(1, Priority.LOW);
		StringRequest request2 = getRequest(2, Priority.LOW);
		StringRequest request3 = getRequest(3, Priority.NORMAL);
		StringRequest request4 = getRequest(4, Priority.IMMEDIATE);
		StringRequest request5 = getRequest(5, Priority.HIGH);
		StringRequest request6 = getRequest(6, Priority.HIGH);

		mQueue.add(request1);
		mQueue.add(request2);
		mQueue.add(request3);
		if (mCancelAfter3Button.isChecked()){
			mQueue.cancelAll(PRIO_CANCEL_TAG);
			mResultAdapter.add("canceled 1, 2, 3");
		}
		mQueue.add(request4);
		mQueue.add(request5);
		mQueue.add(request6);
	}

	private StringRequest getRequest(int i, final Priority prio) {
		final RequestChecker r = mPerformanceDisplayer.start();
		r.setRequestNumber(i);
		StringRequest request = new StringRequest("http://saramak.pl/volleyt/"
				+ i + ".txt", new SuccessListener(mResultAdapter,
				mPerformanceDisplayer, r), new ErrorDisplayListener()) {
			@Override
			public com.android.volley.Request.Priority getPriority() {
				return prio;
			}
		};
		request.setTag(PRIO_CANCEL_TAG);
		return request;
	}

	private static class SuccessListener implements Listener<String> {

		private PerformanceDisplayer pd;
		private RequestChecker r;
		private ArrayAdapter<String> mResultAdapter;

		public SuccessListener(ArrayAdapter<String> resultAdapter,
				PerformanceDisplayer pd, RequestChecker r) {
			this.mResultAdapter = resultAdapter;
			this.pd = pd;
			this.r = r;

		}

		@Override
		public void onResponse(String res) {
			mResultAdapter.add(res.substring(0, 1));
			mResultAdapter.notifyDataSetChanged();
			pd.end(r);
		}

	}

	private static class ErrorDisplayListener implements ErrorListener {

		@Override
		public void onErrorResponse(VolleyError arg0) {

		}

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mListView.setAdapter(mResultAdapter);
			mResultAdapter.notifyDataSetChanged();
		} else {

			mPerformanceDisplayer.showResults(mListView);
			mResultAdapter.notifyDataSetChanged();
		}
	}
}
