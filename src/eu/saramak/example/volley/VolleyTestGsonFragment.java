package eu.saramak.example.volley;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;

import com.android.volley.Cache.Entry;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.gson.reflect.TypeToken;

import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.RequestChecker;
import eu.saramak.example.volleyrequest.GsonRequest;
import eu.saramak.example.volleyrequest.Pet;


public class VolleyTestGsonFragment extends PerformanceBaseFragment implements
		OnClickListener, OnCheckedChangeListener {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestDetailFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;


	private RequestQueue mQueue;
	private ArrayAdapter<String> mPetsAdapter;

	
	private Switch mSwitchButton;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public VolleyTestGsonFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
		VolleyLog.DEBUG= true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_detail_test1,
				container, false);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		
		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);
		
		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mPetsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
		
		mListView.setAdapter(mPetsAdapter);
		mStartButton.setOnClickListener(this);
		
		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);
	
		return rootView;
	}



	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		final RequestChecker r = mPerformanceDisplayer.start();
		String url = mItem.url;//http://damp-chamber-4526.herokuapp.com/pets/clist
		
		
		Type type = new TypeToken<List<Pet>>(){}.getType();
		GsonRequest<List<Pet>> gsonRequest = new GsonRequest<List<Pet>>(url, type, null, new Listener<List<Pet>>() {
			
			@Override
			public void onResponse(List<Pet> list) {
				mPerformanceDisplayer.end(r);
				mPetsAdapter.clear();
				mPetsAdapter.addAll(Collections2.transform(list, new PetToStringFunction()));
				mPetsAdapter.notifyDataSetChanged();
			}

			
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				Log.d(TAG, "error");
			}
			
		});

		mQueue.add(gsonRequest);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mListView.setAdapter(mPetsAdapter);
			mPetsAdapter.notifyDataSetChanged();
		}else {
			
			mPerformanceDisplayer.showResults(mListView);
			mPetsAdapter.notifyDataSetChanged();
		}
	}
}
