package eu.saramak.example.volley;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;

import com.android.volley.Cache.Entry;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import eu.saramak.example.volley.dummy.URLContent;
import eu.saramak.example.volley.performance.RequestChecker;


public class VolleyTestDetailFragment extends PerformanceBaseFragment implements
		OnClickListener, OnCheckedChangeListener {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	protected static final String TAG = "VolleyTestDetailFragment";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private URLContent.DummyItem mItem;

	private Button mStartButton;


	private RequestQueue mQueue;
	private ArrayAdapter<String> mPetsAdapter;

	
	private Switch mSwitchButton;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public VolleyTestDetailFragment() {
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = URLContent.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
		VolleyLog.DEBUG= true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.volley_fragment_detail_test1,
				container, false);
		
		mQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
		
		mStartButton = (Button) rootView.findViewById(R.id.button_start);
		mListView = (ListView) rootView.findViewById(R.id.list_result);
		
		mSwitchButton = (Switch) rootView.findViewById(R.id.switchAdapters);
		mPetsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
		
		mListView.setAdapter(mPetsAdapter);
		mStartButton.setOnClickListener(this);
		
		mSwitchButton.setOnCheckedChangeListener(this);
		mPerformanceDisplayer.onCreateView(rootView);
	
		return rootView;
	}


	private JsonArrayRequest jsRequest;

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		final RequestChecker r = mPerformanceDisplayer.start();
		
		String url = mItem.url;
		jsRequest = new JsonArrayRequest(url, 
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.d(TAG, "response " + response);
						try {
							List<String> petsNames = mPetParser.parse(response);
							mPetsAdapter.clear();
							mPetsAdapter.addAll(petsNames);
						} catch (JSONException e) {
							Log.d(TAG, "error when parsing json array ", e);
						}
						debugCache();
						mPerformanceDisplayer.end(r);
					}

					private void debugCache() {
						Entry cacheEntry = jsRequest.getCacheEntry();
						if (cacheEntry!=null){
						Log.d(TAG, "cache etag " + cacheEntry.etag + " responseHeaders" + cacheEntry.responseHeaders);
						Log.d(TAG, "cacheEntry " + cacheEntry + " + " + cacheEntry.isExpired());
						}else {
							Log.d(TAG, "cacheEntry null" + jsRequest.shouldCache()); 
						}
					}

				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "ErrorListener " + error.getMessage());
					}
				});
		mQueue.add(jsRequest);
		
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			mListView.setAdapter(mPetsAdapter);
			mPetsAdapter.notifyDataSetChanged();
		}else {
			
			mPerformanceDisplayer.showResults(mListView);
			mPetsAdapter.notifyDataSetChanged();
		}
	}
}
