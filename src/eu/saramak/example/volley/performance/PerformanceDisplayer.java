package eu.saramak.example.volley.performance;

import android.view.View;
import android.widget.ListView;

public interface PerformanceDisplayer {

	void onCreateView(View rootView);

	RequestChecker start();

	void showResults(ListView mListView);

	void end(RequestChecker r);

}
