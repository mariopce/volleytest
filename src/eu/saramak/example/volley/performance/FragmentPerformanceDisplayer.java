package eu.saramak.example.volley.performance;

import java.util.concurrent.atomic.AtomicInteger;

import android.os.SystemClock;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import eu.saramak.example.volley.R;

public class FragmentPerformanceDisplayer implements PerformanceDisplayer {
	private TextView mResultTimeView;
	private ArrayAdapter<String> mTimeAdapter;
	
	private long mMax;
	private AtomicInteger requestCount = new AtomicInteger();
	public FragmentPerformanceDisplayer() {
		
	}
	public void onCreateView(View rootView) {
		mResultTimeView = (TextView) rootView.findViewById(R.id.item_detail);
		mResultTimeView.setText("start");
		mTimeAdapter = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1);
	}
	@Override
	public synchronized RequestChecker start() {
		
		
		return new RequestChecker(requestCount.incrementAndGet());
	}
	@Override
	public synchronized void end(RequestChecker r) {
		long end = SystemClock.elapsedRealtimeNanos();
		r.finishHim(end);
		mMax = Math.max(mMax, r.getTime());
		mTimeAdapter.add(r.getReq()+") " + formatMicro(r.getTime())+"ms");
		mResultTimeView.setText(r+" " +  " max: " + formatMicro(mMax));		
	}
	private long formatMicro(long end) {
		return end;
	}
	@Override
	public void showResults(ListView listView) {
		listView.setAdapter(mTimeAdapter);		
	}
}
