package eu.saramak.example.volley.performance;

import android.os.SystemClock;

public class RequestChecker {

	private int requestCount;
	private long start;
	private long end;

	public RequestChecker(int requestCount) {
		this.requestCount = requestCount;
		this.start = SystemClock.elapsedRealtimeNanos();
	}

	public void finishHim(long end) {
		this.end = end;
	}
	public long getTime(){
		return (end - start)/1000000;
	}
	@Override
	public String toString() {
		return "time: " + getTime();
	}

	public String getReq() {
		return ""+requestCount+":";
	}

	public void setRequestNumber(int i) {
		requestCount = i;
	}
}
